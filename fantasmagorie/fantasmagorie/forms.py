import datetime
import requests
import tempfile
import tmdbsimple as tmdb
from fantasmagorie.youtube import youtube_search

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.files import File
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.text import mark_safe, slugify


from . import models

tmdb.API_KEY = settings.TMDB_KEY


class FetchMovieForm(forms.Form):
    movie = forms.CharField(label=_("Movie"), max_length=200)
    fetched_movies = forms.ChoiceField(label=_("Results"), required=False,
                                       choices=[], widget=forms.RadioSelect)

    def __init__(self, *args, **kwargs):
        super(FetchMovieForm, self).__init__(*args, **kwargs)
        self.potential_duplicates = []
        self.created_genres = []
        if not args or not args[0] or not args[0].get('movie', None):
            return
        movie = args[0].get('movie', None)
        search_key = 'tmdb_search_' + slugify(movie)
        choices = cache.get(search_key)
        if not choices:
            choices = self._fetch_movie(movie)
            cache.set(search_key, choices)
        self.fields['fetched_movies'].choices = choices

    def clean(self):
        if not settings.TMDB_KEY:
            raise forms.ValidationError(
                _("Your API key for TheMovieDB is not set"))

    @property
    def config(self):
        config = cache.get('tmdb_config')
        if not config:
            config = tmdb.Configuration().info()
            cache.set('tmdb_config', config)
        return config

    def _fetch_movie(self, movie):
        choices = []
        search = tmdb.Search()
        search.movie(query=movie, language=settings.TMDB_LANGUAGE)
        for s in search.results:
            if s["poster_path"]:
                config_image = self.config['images']
                img_path = config_image['secure_base_url'] + \
                           config_image['poster_sizes'][0] + s["poster_path"]
            else:
                img_path = "{}/default-movie-small.png".format(
                    settings.STATIC_URL)
            img = "<div><img src={}></div> ".format(img_path)
            date = ""
            if s['release_date']:
                date = " ({})".format(s['release_date'])
            val = \
                "<div class='movie-result'>{}<div><p><strong>{}{}</strong></p>" \
                "<p>{}</p></div></div>".format(
                    img, s['title'], date, s['overview'])
            res = mark_safe(val)
            choices.append((s['id'], res))
        return choices

    def fetch(self):
        movie = self.cleaned_data['movie']
        movie = movie.replace("'", " ").replace("’", " ")
        # ’ and ' can be ambiguous in searches
        search_key = 'tmdb_search_' + slugify(movie)
        results = cache.get(search_key)
        if results:
            self.fields['fetched_movies'].choices = results
            return True
        choices = self._fetch_movie(movie)
        cache.set(search_key, choices)
        self.fields['fetched_movies'].choices = choices
        if choices:
            return True

    def save(self):
        movie_pk = self.cleaned_data['fetched_movies']
        data = tmdb.Movies(movie_pk).info(language=settings.TMDB_LANGUAGE)

        base_slug = slugify(data['title'])
        slug = base_slug[:]
        while models.Movie.objects.filter(slug=slug).count():
            self.potential_duplicates += list(
                [str(m) for m in models.Movie.objects.filter(slug=slug).all()])
            if slug == base_slug:
                slug = slug + "-1"
            else:
                slugs = slug.split('-')
                slug = "-".join(slugs[:-1]) + "-" + str(int(slugs[-1]) + 1)

        duration = None
        if data['runtime']:
            duration = datetime.timedelta(minutes=int(data['runtime']))

        description = data['overview']

        values = {
            'name': data['title'],
            'slug': slug,
            'duration': duration,
            'description': description,
        }
        movie = models.Movie.objects.create(**values)

        if data['poster_path']:
            config_image = self.config['images']
            img_url = config_image['secure_base_url'] + \
                config_image['poster_sizes'][-1] + data["poster_path"]
            request = requests.get(img_url, stream=True)
            if request.status_code == requests.codes.ok:
                filename = slug + "." + img_url.split('.')[-1]

                lf = tempfile.NamedTemporaryFile()
                # read the streamed image in sections
                for block in request.iter_content(1024 * 8):
                    if not block:
                        break
                    lf.write(block)
                movie.poster.save(filename, File(lf))

        for genre in data['genres']:
            genre, created = models.Genre.objects.get_or_create(
                slug=slugify(genre['name']),
                defaults={'name': genre['name'], 'color':
                    models.ColorSet.objects.all()[0]}
            )
            if created:
                self.created_genres.append(str(genre))
            movie.genres.add(genre)

        return movie


class SearchMovieForm(forms.Form):
    query = forms.CharField(label=_("Search"), max_length=200)
    extra_query = forms.ChoiceField(label=_("Extra query"), required=False,
                                    choices=[('', '--')])
    videos = forms.ChoiceField(label=_("Results"), required=False,
                               choices=[], widget=forms.RadioSelect)

    def __init__(self, *args, **kwargs):
        choices = []
        for sq in models.SearchQuery.objects.filter(available=True).all():
            choices.append((sq.query, sq.name))
            if sq.default:
                if 'initial' not in kwargs:
                    kwargs['initial'] = {}
                kwargs['initial']['extra_query'] = sq.query
        super(SearchMovieForm, self).__init__(*args, **kwargs)
        self.fields['extra_query'].choices += choices
        if not args or not args[0] or not args[0].get('query', None):
            return
        q = args[0].get('query', '')
        xq = args[0].get('extra_query', '')
        if not q:
            return
        q += " " + xq
        choices = self._search_movie(q)
        self.fields['videos'].choices = choices

    def _search_movie(self, query):
        if not settings.YOUTUBE_KEY:
            return []
        choices = []
        results = youtube_search(settings.YOUTUBE_KEY, query, 6)
        for title, youtube_id in results:
            val = """
            <div class='yt-result'>
            <span class='yt-title'>{}</span>
            <iframe width="444" height="250"
                src="https://www.youtube.com/embed/{}" frameborder="0"
                allowfullscreen>
            </iframe>
            </div>
            """.format(title, youtube_id)
            res = mark_safe(val)
            choices.append((youtube_id, res))
        return choices


class SubscribeForm(forms.Form):
    name = forms.CharField(label=_("Name"), max_length=30,
                           help_text=_("Optional."), required=False)
    email = forms.EmailField(max_length=254,
                             help_text=_('Required. Inform a valid email '
                                         'address.'))
    question = forms.CharField(label=_("No robots here! What is the movie "
                                       "theater name?"), max_length=254)
    valid = forms.BooleanField(
        label=_("By submitting this form, I accept that theses "
                "pieces of information will be used for sending the movie "
                "theater newsletter."))

    def __init__(self, *args, **kwargs):
        self.theater = kwargs.pop('theater')
        super(SubscribeForm, self).__init__(*args, **kwargs)

    def clean_question(self):
        response = slugify(self.cleaned_data['question'].lower(
            ).replace("l'", "")).replace('cinema-', '').replace('cinma-', '')
        if response != self.theater.slug:
            raise ValidationError(_("Mr. Robot? If you are not, change your "
                                    "response to the question, it shouldn't "
                                    "be that hard."))


class SignUpForm(SubscribeForm, UserCreationForm):
    email = forms.EmailField(max_length=254,
                             help_text=_('Required. Inform a valid email '
                                         'address.'))
    question = forms.CharField(label=_("No robots here! What is the movie "
                                       "theater name?"), max_length=254)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


class DeleteAccountForm(forms.Form):
    sure = forms.NullBooleanField(
        label=_("Are you sure you want to delete your account?"), required=True)


class UnsubscribeForm(forms.Form):
    email = forms.EmailField(label=_("Email"), required=True)
    sure = forms.BooleanField(label=_("Are you sure you want to "
                                      "unsubscribe?"), required=True)
