# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0013_auto_20170803_1206'),
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=60)),
                ('available', models.BooleanField(verbose_name='Available', default=True)),
                ('order', models.IntegerField(default=1, choices=[(1, 'first'), (2, 'second'), (3, 'third'), (4, 'fourth'), (5, 'fifth'), (6, 'sixth'), (7, 'seventh'), (8, 'eighth'), (9, 'ninth'), (10, 'tenth')])),
                ('theater', models.ForeignKey(to='fantasmagorie.MovieTheater')),
            ],
            options={
                'verbose_name': 'Menu',
                'verbose_name_plural': 'Menus',
                'ordering': ('theater', 'order'),
            },
        ),
        migrations.CreateModel(
            name='MenuPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=1, choices=[(1, 'first'), (2, 'second'), (3, 'third'), (4, 'fourth'), (5, 'fifth'), (6, 'sixth'), (7, 'seventh'), (8, 'eighth'), (9, 'ninth'), (10, 'tenth')])),
                ('menu', models.ForeignKey(to='fantasmagorie.Menu')),
                ('page', models.ForeignKey(to='fantasmagorie.FlatPage')),
            ],
            options={
                'verbose_name': 'Menu page',
                'verbose_name_plural': 'Menu pages',
                'ordering': ('menu', 'order'),
            },
        ),
    ]
