import datetime
import os
from PIL import Image, ImageDraw, ImageFont

from django.conf import settings
from django.utils.formats import dateformat
from django.utils.translation import ugettext_lazy as _

from .models import MovieTheater

BASE_RATIO = (210, 118)
POSTER_SIZE = (BASE_RATIO[0] * 8, BASE_RATIO[1] * 8)


def create_image_for_movie(movie, theater=None, date=None):
    if not date:
        date = datetime.date.today() - datetime.timedelta(days=1000)
    q = movie.broadcasts.filter(date__gte=date)
    if not q.count():
        print(1)
        return
    if not movie.poster:
        print(2)
        return
    img = Image.open(settings.MEDIA_ROOT + movie.poster.name)
    width, height = img.size

    new_width = int(height * BASE_RATIO[0] / BASE_RATIO[1])
    new_img = Image.new("RGB", (new_width, height), "black")
    new_img.paste(img, (0, 0))
    new_img = new_img.resize(POSTER_SIZE)
    width, height = new_img.size

    d = ImageDraw.Draw(new_img)

    font = ImageFont.truetype(settings.FONT_FOR_IMAGE, 48, encoding="unic")

    if not theater:
        theater = MovieTheater.objects.all()[0]

    text = theater.name
    d.text((width / 2 - 80, 220), text, fill=(255, 255, 255), font=font)

    font = ImageFont.truetype(settings.FONT_FOR_IMAGE, 36, encoding="unic")

    text, text2 = "", ""
    for broadcast in q.order_by('date').all():
        text += dateformat.format(broadcast.date, "l d F Y").capitalize()
        extra = []
        if broadcast.original_version:
            extra.append(str(_("OV")))
        if broadcast.three_dimensional:
            extra.append(str(_("3D")))
        if extra:
            text += "({})".format(" ".join(extra))
        text2 += str(broadcast.schedule)
    d.text((width / 2 - 80, 380), text, fill=(255, 255, 255), font=font)
    d.text((width / 2 + 640, 380), text2, fill=(255, 255, 255), font=font)
    tmp_path = '/tmp/'
    img_path = tmp_path + 'test.jpg'
    new_img.save(img_path)
    cmd = "ffmpeg -f image2  -loop 1  -t 10 -i {} -c:v libx264 "\
          "-pix_fmt yuv420p -y {}/video.mp4".format(img_path, tmp_path)
    os.system(cmd)
