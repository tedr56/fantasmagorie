# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0003_movie_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='movietheater',
            name='extra_foot',
            field=models.TextField(blank=True, null=True, verbose_name='Extra foot mention'),
        ),
    ]
