import datetime
import logging

from django.conf import settings
from django_extensions.management.jobs import DailyJob
from django.utils import translation

from newsletter.models import Message
from fantasmagorie import models, utils

logger = logging.getLogger(__name__)


def get_next_mailing_slugs(day_limit=3, theater_filter={}):
    translation.activate(settings.LANGUAGE_CODE)
    d = datetime.timedelta
    today = datetime.date.today()
    today_wd = today.weekday()

    slugs = []

    for theater in models.MovieTheater.objects.filter(**theater_filter).all():
        next_wd_movie = int(theater.weekly_mailing_weekday)
        if today_wd >= next_wd_movie:
            delta = 6 - today_wd + 1 + next_wd_movie
        else:  # the current week
            delta = next_wd_movie - today_wd
        if delta > day_limit:  # do not create it if before day_limit days
            continue
        next_first_movie_day = today + d(days=delta)
        next_last_movie_day = next_first_movie_day + d(days=7)
        slug = theater.slug + "-auto-" + next_first_movie_day.strftime(
            "%Y-%m-%d")
        slugs.append((theater, slug, next_first_movie_day, next_last_movie_day))
    return slugs


class Job(DailyJob):
    help = "Auto-create daily mailing."

    def execute(self):
        for theater, slug, next_first_movie_day, next_last_movie_day \
                in get_next_mailing_slugs(
                    day_limit=3,
                    theater_filter={'autocreate_weekly_mailing': True}):
            if Message.objects.filter(slug=slug).count():
                # already created
                continue
            movies = models.Movie.objects.filter(
                broadcasts__screen__movie_theater=theater,
                broadcasts__date__gte=next_first_movie_day,
                broadcasts__date__lt=next_last_movie_day).distinct()
            utils.create_mailing_from_movies(
                movies, theater, date=next_first_movie_day, slug=slug)


