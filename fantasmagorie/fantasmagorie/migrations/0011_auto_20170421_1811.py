# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
        ('fantasmagorie', '0010_movietheater_side'),
    ]

    operations = [
        migrations.CreateModel(
            name='FlatPage',
            fields=[
                ('flatpage_ptr', models.OneToOneField(to='flatpages.FlatPage', parent_link=True, serialize=False, primary_key=True, auto_created=True)),
                ('page_type', models.CharField(verbose_name='Type', choices=[('html', 'HTML'), ('md', 'Markdown')], default='html', max_length=10)),
            ],
            bases=('flatpages.flatpage',),
        ),
        migrations.AlterField(
            model_name='movietheater',
            name='footer',
            field=models.ForeignKey(to='fantasmagorie.FlatPage', verbose_name='Footer', related_name='theater_footers', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='movietheater',
            name='intro',
            field=models.ForeignKey(to='fantasmagorie.FlatPage', verbose_name='Intro', related_name='theater_intros', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='movietheater',
            name='pages',
            field=models.ManyToManyField(to='fantasmagorie.FlatPage', blank=True, related_name='theaters', through='fantasmagorie.TheaterPage', verbose_name='Pages'),
        ),
        migrations.AlterField(
            model_name='movietheater',
            name='side',
            field=models.ForeignKey(to='fantasmagorie.FlatPage', verbose_name='Side', related_name='theater_sides', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='theaterpage',
            name='page',
            field=models.ForeignKey(to='fantasmagorie.FlatPage'),
        ),
    ]
