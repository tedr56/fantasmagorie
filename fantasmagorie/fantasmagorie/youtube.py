from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow

YT_PLAYLIST = "https://www.youtube.com/embed?list="
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"
SCOPES = ['https://www.googleapis.com/auth/youtube']


def get_service(developer_key):
    return build(
        YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=developer_key)


def get_oauth_service(client_secret_file):
    flow = InstalledAppFlow.from_client_secrets_file(client_secret_file,
                                                     SCOPES)
    credentials = flow.run_console()
    return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                 credentials=credentials)


def youtube_search(developer_key, query, max_results=25):
    youtube = get_service(developer_key)

    search_response = youtube.search().list(
        q=query, part="id,snippet", maxResults=max_results
    ).execute()

    videos = []

    for search_result in search_response.get("items", []):
        if search_result["id"]["kind"] == "youtube#video":
            videos.append((search_result["snippet"]["title"],
                           search_result["id"]["videoId"]))

    return videos


def youtube_get_channels(service):
    return service.channels().list(
        part='snippet,contentDetails,statistics',
        mine=True).execute()


def youtube_create_playlist(yt_client, name, description='',
                            videos_ids=None):
    if not videos_ids:
        return

    body = {
        'snippet': {
            "title":name,
            "description":description
        },
        'status': {
            'privacyStatus': 'public'
        }
    }
    try:
        playlist = yt_client.playlists().insert(
            part='snippet,status', body=body
        ).execute()
    except HttpError:
        return

    base_body = {
        'snippet': {
            'playlistId': playlist['id'],
            'resourceId': {'kind': 'youtube#video',
                           'videoId': ''},
        }
    }

    for video_id in videos_ids:
        body = base_body.copy()
        body['snippet']['resourceId']['videoId'] = video_id
        try:
            yt_client.playlistItems().insert(
                body=body,
                part='snippet',
            ).execute()
        except HttpError:
            continue

    return playlist['id']


def create_playlist_from_broadcasts(yt_client, broadcasts, title, desc):
    from fantasmagorie.models import resource_filter

    video_ids, movies = [], []
    regexps, lnk = resource_filter['Youtube']

    for broadcast in broadcasts:
        movie = broadcast.movie
        if movie.pk in movies:
            continue
        movies.append(movie.pk)
        yt_videos = movie.external_resources.filter(
            resource_type='Youtube'
        )
        if not yt_videos.count():
            continue

        for yt_video in yt_videos:
            key = None
            for regexp in regexps:
                key = regexp.findall(yt_video.link)
                if key:
                    key = key[0]
                    break
            if not key:
                continue
            video_ids.append(key)
    if not video_ids:
        return

    playlist_id = youtube_create_playlist(
        yt_client, title, desc or "", videos_ids=video_ids
    )
    if not playlist_id:
        return

    return playlist_id

