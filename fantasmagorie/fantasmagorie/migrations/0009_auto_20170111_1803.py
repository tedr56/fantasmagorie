# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0008_auto_20170110_1218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='file',
            field=models.FileField(verbose_name='File', upload_to='documents/%Y/%m/'),
        ),
        migrations.AlterField(
            model_name='event',
            name='picture',
            field=models.ImageField(verbose_name='Picture', upload_to='events/%Y/%m/', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='image',
            name='image',
            field=models.ImageField(verbose_name='Image', upload_to='images/%Y/%m/'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='poster',
            field=models.ImageField(verbose_name='Poster', upload_to='posters/%Y/%m/', null=True, blank=True),
        ),
    ]
