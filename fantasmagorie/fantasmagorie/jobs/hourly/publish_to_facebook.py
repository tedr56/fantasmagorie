import datetime
import logging

from django_extensions.management.jobs import HourlyJob

from django.utils.translation import ugettext as _
from fantasmagorie import models

logger = logging.getLogger(__name__)


class Job(HourlyJob):
    help = "Auto-publish to Facebook."

    def execute(self):
        logger.info(_('Auto-publish to Facebook.'))
        d = datetime.timedelta
        for theater in models.MovieTheater.objects.all():
            if not theater.facebook_page_access_token or \
                    not theater.autopublish_day_delay:
                continue
            today = datetime.date.today()
            now = datetime.datetime.now()

            before = today + d(days=theater.autopublish_day_delay)
            before_time = now + d(days=theater.autopublish_day_delay)
            for broadcast in models.Broadcast.objects.filter(
                    facebook_published=False,
                    date__gte=today, date__lte=before).all():
                d = datetime.datetime.combine(broadcast.date,
                                              broadcast.schedule.time)
                if d <= before_time:
                    broadcast.publish_to_facebook()