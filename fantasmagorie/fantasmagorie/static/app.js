
$.fn.center = function () {
    this.css("position","absolute");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
}


var display_link = function(){
    $(this).next('.hidden-link').show();
    $(this).unbind('click');
    $(this).click(function(){
        $(this).next('.hidden-link').hide();
        $(this).unbind('click');
        $(this).click(display_link);
        return false;
    });
    return false;
}

var register_reveals = function(){

    $(".reveal").unbind("closed.zf.reveal");
    $('.reveal').on('closed.zf.reveal', function() {
        $(this).html('');
    });

    $("a[data-link='movie']").unbind("click");
    $("a[data-link='movie']").click(function(){
        var url = "/movie/" + $(this).attr('data-movie') + "/" +
            $(this).attr('data-broadcast-id') + "/";
        $.ajax(url).done(function(data) {
            $("#movie").html(data).foundation("open");
        });
    });

    // link inside an event
    $(".nested-movie-link").unbind("click");
    $(".nested-movie-link").click(function(){
        var url = "/movie/" + $(this).attr('data-movie') + "/" +
            $(this).attr('data-broadcast-id') + "/";
        $.ajax(url).done(function(data) {
            $("#nested-movie").html(data).foundation("open");
        });
    });

    $(".event-link").unbind("click");
    $(".event-link").click(function(){
        var url = "/event/" + $(this).attr('data-event') + "/";
        $.ajax(url).done(function(data) {
            $("#event").html(data).foundation("open");
        });
    });

    $(".page-link").unbind("click");
    $(".page-link").click(function(){
        var url = "/pages" + $(this).attr('data-page');
        $.ajax(url).done(function(data) {
            $("#page").html(data).foundation("open");
        });
    });

    register_staticpage();

    $('.display-link').unbind('click');
    $('.display-link').click(display_link);
};

var register_staticpage = function(){
    $(".staticpage-link").unbind("click");
    $(".staticpage-link").click(function(){
        var url = $(this).attr('data-page');
        $.ajax(url).done(function(data) {
            $("#tiny-page").html(data).foundation("open");
        });
    });


    $("form.staticpage-form").submit(function(e) {
      var data = $(this).serialize();
      var url = $(this).attr("action");
      var form = $(this);
      $.post(url, data, function(data) {
        try {
            //data = JSON.parse(data);
		    //$(.result).html(data.result + " Watchlist");
            $("#tiny-page").html(data);

        } catch (e) {
            console.log("json encoding failed");
            return false;
        }
      });
      return false;
    });

}

$(document).ready(function() {
    register_reveals();
    $(".title-bar-title").click(function(){
        $(".menu-icon").click();
    });
});

