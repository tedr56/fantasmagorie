#! /usr/bin/env python3

import facebook
import requests

short_token = input('* token: ').strip()
app_id = input('* app ID: ').strip()
secret = input('* app secret: ').strip()


LONG_LIVED_TOKEN_URL = "https://graph.facebook.com/oauth/access_token?"\
    "client_id={APP_ID}&"\
    "client_secret={APP_SECRET}&"\
    "grant_type=fb_exchange_token&"\
    "fb_exchange_token={SHORT_ACCESS_TOKEN}".format(
      SHORT_ACCESS_TOKEN=short_token,
      APP_ID=app_id,
      APP_SECRET=secret)

response = requests.get(LONG_LIVED_TOKEN_URL).json()
# print(response)

long_token = response["access_token"]

graph = facebook.GraphAPI(long_token)
resp = graph.get_object('me/accounts')
page_access_token = None
for page in resp['data']:
    print("\"{}\" ({}): {}".format(page['name'], page['category'],
                                   page['access_token']))
