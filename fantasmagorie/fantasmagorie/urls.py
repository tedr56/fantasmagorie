from django.conf import settings
from django.conf.urls import url, include
from django.views.static import serve
from django.contrib.auth import views as auth_views
from . import views

from .feeds import LatestEntriesFeed


re_date = r'[12][09][0-9]{2}-[01][0-9]-[0-3][0-9]'

urlpatterns = [
    url(r'^api/broadcasts/'
        r'(?:(?P<start_date>' + re_date + ')/'
        r'(?:(?P<end_date>' + re_date + ')/)?'
        r' )?$',
        views.ApiBroadcastListRetrieve.as_view(),
        name='api-broadcast-list'),
    url(r'^api/movie/(?P<slug>[-\w]+)/$', views.ApiMovieDetail.as_view(),
        name='api-movie-detail'),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]

if settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns


urlpatterns += [
    url(r'^newsletter/', include('newsletter.urls')),
    url(r'^status/$', views.status, name='status'),
    url(r'^(?P<theater_slug>[-\w]+)/latest/feed/$', LatestEntriesFeed(),
        name="feed"),
    url(r'^youtube-authorize/', views.AuthorizeView.as_view(),
        name='authorize'),
    url(r'^oauth2callback/', views.Oauth2CallbackView.as_view(),
        name='oauth2callback'),

    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),

    url(r'^(?:(?P<theater>[-\w]+)/)?signup/',
        views.signup, name='signup'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'newsletter-login/'},
        name='logout'),
    url(r'^delete-account/$', views.delete_account, name='delete-account'),
    url(r'^f_password_reset/$', views.password_reset,
        name='f_password_reset'),
    url(r'^password_reset/done/$', views.password_reset_done,
        name='f_password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete,
        name='password_reset_complete'),
    url(r'^unsubscribe/(?:(?P<email>.*)/)?$', views.unsubscribe,
        name='unsubscribe'),
    url(r'^subscribe/$', views.subscribe, name='subscribe'),

    url(r'^(?:(?P<theater>[-\w]+)/)?newsletter-manager/',
        views.newsletter_manager, name='newsletter_manager'),
    url(r'^(?:(?P<theater>[-\w]+)/)?newsletter-login/(?P<first>first)?',
        views.newsletter_login, name='newsletter_login'),

    url(r'^(?:(?P<theater>[-\w]+)/)?newsletter-redirect/',
        views.NewsletterRedirect.as_view(), name='newsletter-redirect'),

    url(r'^(?:(?P<theater>[-\w]+)/)?page(?P<url>/[-\w]+/)',
        views.PageLink.as_view(), name='flatpage-link'),
    url(r'^(?:(?P<theater>[-\w]+)/)?pages(?P<url>/[-\w]+/)',
        views.FlatPageView.as_view(), name='flatpage'),
    url(r'^(?:(?P<theater>[-\w]+)/)?movie/(?P<slug>[-\w]+)/'
        r'(?P<broadcast_id>[0-9]+)/event.ics',
        views.ICSMovieView.as_view(), name='movie-ical'),
    url(r'^(?:(?P<theater>[-\w]+)/)?movie/(?P<slug>[-\w]+)/'
        r'(?:(?P<broadcast_id>[0-9]+)/)?',
        views.MovieDetail.as_view(), name='movie-detail'),
    url(r'^(?:(?P<theater>[-\w]+)/)?movie-link/(?P<slug>[-\w]+)/'
        r'(?:(?P<broadcast_id>[0-9]+)/)?',
        views.MovieDetailLink.as_view(), name='movie-detail-link'),
    url(r'^(?:(?P<theater>[-\w]+)/)?event/(?P<slug>[-\w]+)/',
        views.EventDetail.as_view(), name='event-detail'),
    url(r'^(?:(?P<theater>[-\w]+)/)?event-link/(?P<slug>[-\w]+)/',
        views.EventDetailLink.as_view(), name='event-detail-link'),
    url(r'^(?:(?P<theater>[-\w]+)/(?:(?P<current_date>\d{4}-\d{2}-\d{2}))?)?',
        views.Index.as_view(), name='index'),
]
