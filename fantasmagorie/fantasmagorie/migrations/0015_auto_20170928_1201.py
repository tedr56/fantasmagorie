# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0014_menu_menupage'),
    ]

    operations = [
        migrations.AddField(
            model_name='menu',
            name='slug',
            field=models.SlugField(unique=True, help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', default='default'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='menu',
            name='theater',
            field=models.ForeignKey(to='fantasmagorie.MovieTheater', related_name='menus'),
        ),
        migrations.AlterField(
            model_name='menupage',
            name='menu',
            field=models.ForeignKey(to='fantasmagorie.Menu', related_name='pages'),
        ),
    ]
