import datetime
import icalendar as ical
import os
from urllib.parse import quote_plus, unquote_plus

from rest_framework import generics
from rest_framework.response import Response
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout, views as auth_views
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import Http404, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.views.generic import TemplateView, View

from newsletter.models import Subscription


from .tokens import account_activation_token
from . import models, serializers, context_processors, forms

from oauth2client.client import flow_from_clientsecrets
from oauth2client.contrib.django_util.storage import DjangoORMStorage
from oauth2client.contrib import xsrfutil


FLOW = {}


def get_flow(request):
    if not settings.GOOGLE_OAUTH2_CLIENT_SECRETS_JSON:
        return
    if FLOW.get('auth', None) or not settings.GOOGLE_OAUTH2_CLIENT_SECRETS_JSON:
        return FLOW['auth']
    protocol = ('https' if request.is_secure() else 'http') + "://"
    url = protocol + get_current_site(request).domain + '/oauth2callback/'
    FLOW['auth'] = flow_from_clientsecrets(
        settings.GOOGLE_OAUTH2_CLIENT_SECRETS_JSON,
        scope='https://www.googleapis.com/auth/youtube',
        redirect_uri=url)
    return FLOW['auth']


class AuthorizeView(View):
    def get(self, request, *args, **kwargs):
        flow = get_flow(request)
        if not flow:
            raise Http404()
        storage = DjangoORMStorage(
            models.CredentialsModel, 'id', request.user.id, 'credential')
        credential = storage.get()

        if credential is None or credential.invalid is True:
            flow.params['state'] = xsrfutil.generate_token(
                settings.SECRET_KEY, request.user)
            authorize_url = flow.step1_get_authorize_url()
            return redirect(authorize_url)
        return redirect('/admin/fantasmagorie/movie/')


class Oauth2CallbackView(View):
    def get(self, request, *args, **kwargs):
        if not xsrfutil.validate_token(
                settings.SECRET_KEY, request.GET.get('state').encode(),
                request.user):
            return HttpResponseBadRequest()
        flow = get_flow(request)
        if not flow:
            raise Http404()
        credential = flow.step2_exchange(request.GET)
        storage = DjangoORMStorage(
            models.CredentialsModel, 'id', request.user.id, 'credential')
        storage.put(credential)
        return redirect('/admin/fantasmagorie/movie/')


class Day:
    def __init__(self, date, broadcasts, new_week=False, new_month=False,
                 new_year=False, old=False):
        self.date = date
        self.broadcasts = broadcasts
        self.day = self.date.day
        self.week = int(self.date.isocalendar()[1])
        self.month = self.date.month
        self.year = self.date.year
        self.new_week = new_week
        self.new_month = new_month
        self.new_year = new_year
        self.old = old


def status(request):
    return HttpResponse('OK')


def get_theater(kwargs):
    theater = None
    if kwargs.get('theater', None):
        q = models.MovieTheater.objects.filter(slug=kwargs["theater"])
        if q.count():
            theater = q.all()[0]
    if not theater:
        q = models.MovieTheater.objects.filter(default=True)
        if q.count():
            theater = q.all()[0]
        else:
            raise Http404(_("At least one default movie theater have to "
                            "be defined"))
    return theater


class TheaterView:
    def get_theater(self):
        self.theater = get_theater(self.kwargs)
        return self.theater


def get_main_context(theater):
    dct = {
        'theater': theater
    }
    if theater:
        extra_path = os.path.join(settings.MEDIA_ROOT, theater.slug)
        if os.path.isdir(extra_path):
            css = os.path.join(extra_path, "styles.css")
            if os.path.isfile(css):
                dct['extra_css'] = theater.slug + "/styles.css"
            favicon = os.path.join(extra_path, "favicon.ico")
            if os.path.isfile(favicon):
                dct['favicon'] = theater.slug + "/favicon.ico"
        if theater.footer:
            dct['footer'] = theater.footer.content
        if theater.intro:
            dct['intro'] = theater.intro.content
        if theater.side:
            dct['side'] = theater.side.content

    dct['MEDIA_URL'] = settings.MEDIA_URL
    pages = list(
        models.TheaterPage.objects.filter(
            theater=theater).all()) \
            + list(models.TheaterLink.objects.filter(
        theater=theater).all()) \
            + list(models.Menu.objects.filter(
        theater=theater, available=True).all())
    dct['pages'] = sorted(pages, key=lambda page: page.order)
    dct['colors'] = [
        color for color in models.ColorSet.objects.filter(
            available=True).all()]
    return dct


class Index(TemplateView, TheaterView):
    template_name = "index.html"

    def get_main_context(self):
        return get_main_context(self.theater)

    def get_current_date(self, today, current_broadcast=None):
        current_date = today
        forced_current_date = self.kwargs.get('current_date', None)
        if forced_current_date:
            try:
                year, month, day = self.kwargs['current_date'].split('-')
                current_date = datetime.date(int(year), int(month), int(day))
            except ValueError:
                pass
        elif current_broadcast:
            current_date = current_broadcast.date
        else:
            # try to get the first broadcast in the future
            broadcasts = models.Broadcast.objects.filter(
                movie__available=True, date__gte=current_date)
            if broadcasts.count():
                current_date = broadcasts.all()[0].date
            else:
                # no future broadcast try to get the last
                q = models.Broadcast.objects.filter(
                    movie__available=True).order_by('-date')
                if not q.count():
                    # no film in this db!
                    self.film_in_db = False
                else:
                    last_known_broadcast = q.all()[0]
                    end_date = last_known_broadcast.date
                    current_date = end_date - datetime.timedelta(
                        self.theater.max_day_to_display)

        return current_date

    def get_events(self, current_date):
        q = models.Event.objects.filter(
                Q(start_date__gte=current_date) |
                Q(end_date__gte=current_date)
            ).filter(available=True)
        dct = {
            'events': q,
            'events_with_picture': q.filter(
                picture__isnull=False).exclude(picture='').count(),
            'events_with_description': q.filter(
                description__isnull=False).exclude(description='').count()
        }
        return dct

    def get_broadcasts(self, current_date):
        dct = {}
        if not self.film_in_db:
            dct['calendar'] = []
            return dct

        # retrieve the whole week
        start_date = current_date - datetime.timedelta(current_date.weekday())
        end_date = start_date + datetime.timedelta(
            self.theater.max_day_to_display + current_date.weekday())

        dct['previous_broadcasts_link'] = self.get_previous_broadcasts(
            start_date)
        dct['next_broadcasts_link'] = self.get_next_broadcasts(
            end_date)

        broadcasts = models.Broadcast.objects.filter(
            movie__available=True, date__gte=start_date, date__lte=end_date)

        previous_day = None
        calendar = []
        for broadcast in broadcasts:
            # first broadcast
            if not calendar:
                # fill with previous day of the week
                for weekday in range(broadcast.date.weekday()):
                    date = broadcast.date - datetime.timedelta(
                        days=broadcast.date.weekday() - weekday)
                    new_week, new_month, new_year = False, False, False
                    if weekday == 0:
                        new_week = True
                        new_month = True
                        new_year = True
                    else:
                        new_month = date.month != previous_day.date.month
                        new_year = date.year != previous_day.date.year
                    calendar.append(Day(date, [], new_week, new_month,
                                        new_year, old=date < current_date))
                    previous_day = calendar[-1]
                new_week, new_month, new_year = False, False, False
                if not calendar:
                    new_week = True
                    new_month = True
                    new_year = True
                calendar.append(
                    Day(broadcast.date, [broadcast], new_week, new_month,
                        new_year, old=broadcast.date < current_date))
                continue
            previous_day = calendar[-1]
            if broadcast.date == previous_day.date:
                # broadcast the same day
                previous_day.broadcasts.append(broadcast)
                continue
            # not the same week
            if broadcast.date.isocalendar()[1] \
                    != previous_day.date.isocalendar()[1]:
                # finish the previous week
                for delta in range(6 - previous_day.date.weekday()):
                    date = previous_day.date + datetime.timedelta(
                        days=delta + 1)
                    new_month = date.month != previous_day.date.month
                    # new_year info is kept as long as the first week
                    # of the year is not started
                    new_year = previous_day.new_year or \
                               date.year != previous_day.date.year
                    calendar.append(Day(date, [], False, new_month,
                                        new_year, old=date < current_date))
                    previous_day = calendar[-1]

                # fill with previous day of the week
                for weekday in range(broadcast.date.weekday()):
                    date = broadcast.date - datetime.timedelta(
                        days=broadcast.date.weekday() - weekday)
                    new_week = False
                    new_month = date.month != previous_day.date.month
                    new_year = date.year != previous_day.date.year
                    if weekday == 0:
                        new_week = True
                        # a monday for the new year - the info is
                        # repeated
                        if previous_day.new_year:
                            new_year = True
                    calendar.append(Day(date, [], new_week, new_month,
                                        new_year, old=date < current_date))
                    previous_day = calendar[-1]
            else:
                # fill with day between the two broadcasts
                delta_days = broadcast.date.weekday() - \
                             previous_day.date.weekday()
                if delta_days > 1:
                    for delta in range(delta_days - 1):
                        date = previous_day.date + datetime.timedelta(
                            days=1)
                        new_month = date.month != previous_day.date.month
                        new_year = date.year != previous_day.date.year
                        calendar.append(
                            Day(date, [], False, new_month, new_year,
                                old=date < current_date))
                        previous_day = calendar[-1]
            new_week = broadcast.date.isocalendar()[1] \
                       != previous_day.date.isocalendar()[1]
            new_month = broadcast.date.month != previous_day.date.month
            new_year = broadcast.date.year != previous_day.date.year
            # a monday for the new year - the info is repeated
            if new_week and previous_day.new_year:
                new_year = True
            calendar.append(Day(broadcast.date, [broadcast],
                                new_week, new_month, new_year,
                                old=broadcast.date < current_date))
        dct['calendar'] = calendar
        return dct

    def get_previous_broadcasts(self, start_date):
        previous_broadcasts = models.Broadcast.objects.filter(
            movie__available=True, date__lt=start_date).order_by('-date')
        if previous_broadcasts.count():
            previous_broadcast = previous_broadcasts.all()[0]
            previous_start_date = previous_broadcast.date - datetime.timedelta(
                self.theater.max_day_to_display)
            return reverse('index', kwargs={
                'theater': self.theater.slug,
                'current_date': previous_start_date.strftime('%Y-%m-%d')})

    def get_next_broadcasts(self, end_date):
        next_broadcasts = models.Broadcast.objects.filter(
            movie__available=True, date__gt=end_date).order_by('date')
        if next_broadcasts.count():
            next_broadcast = next_broadcasts.all()[0]
            return reverse('index', kwargs={
                'theater': self.theater.slug,
                'current_date': next_broadcast.date.strftime('%Y-%m-%d')})

    def get_movie_context(self):
        return {}

    def get_page_detail(self):
        return {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        self.get_theater()
        context.update(self.get_movie_context())
        context.update(self.get_main_context())

        self.film_in_db = True
        context['today'] = datetime.date.today()
        current_date = self.get_current_date(
            context['today'], context.get('current_broadcast', None))

        context.update(self.get_events(current_date))
        context.update(self.get_broadcasts(current_date))
        context.update(self.get_page_detail())

        # # movies
        return context


class PageMixin(TheaterView):
    def get_page_detail(self):
        context = {}
        theater = self.get_theater()
        context['theater'] = theater
        url = self.kwargs.get('url', None)
        q = Q(theaters=theater, url=url) | Q(
            menus__menu__theater=theater, menus__menu__available=True,
            url=url
        )
        q = models.FlatPage.objects.filter(q)
        if not q.count():
            raise Http404(_("Page not available for this movie theater."))
        page = q.all()[0]
        context['page'] = page
        if page.menus.count():
            context['menu'] = page.menus.all()[0].menu.name

        if hasattr(self.request.user, 'is_staff'):
            context['is_staff'] = self.request.user.is_staff
        return context


class FlatPageView(TemplateView, PageMixin):
    template_name = "flatpage.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context.update(self.get_page_detail())
        return context


class PageLink(PageMixin, Index):
    pass


class MovieDetailMixin(TheaterView):
    def get_movie_context(self):
        context = {}
        theater = self.get_theater()
        context['theater'] = theater
        q = models.Movie.objects.filter(available=True, slug=self.kwargs.get(
            'slug', None))
        if not q.count():
            raise Http404(_("This movie is not available."))
        context['movie'] = q.all()[0]
        broadcast_id = self.kwargs.get('broadcast_id', None)
        context['current_broadcast'] = None
        if broadcast_id:
            q = models.Broadcast.objects.filter(
                pk=broadcast_id,
                movie=context['movie'])
            if q.count():
                context['current_broadcast'] = q.all()[0]
        else:
            q = models.Broadcast.objects.filter(
                date__gte=datetime.date.today(),
                movie=context['movie'])
            if q.count():
                # get the next broadcast
                context['current_broadcast'] = q.order_by('date').all()[0]
            else:
                # no broadcast to come for this movie
                q = models.Broadcast.objects.filter(
                    movie=context['movie'])
                if q.count():
                    # but old broadcast - get the younger one
                    context['current_broadcast'] = q.order_by('-date').all()[0]

        context['previous_broadcast'] = self.get_previous_broadcast(
            context['current_broadcast'], theater)

        context['next_broadcast'] = self.get_next_broadcast(
            context['current_broadcast'], theater)

        context['share_message'] = _("Broadcast of {0} at {1}").format(
            context['movie'], context['theater']
        )
        if hasattr(self.request.user, 'is_staff'):
            context['is_staff'] = self.request.user.is_staff
        return context

    def get_previous_broadcast(self, current_broadcast, theater):
        if not current_broadcast:
            return
        base_q = models.Broadcast.objects.filter(
            movie__available=True,
            screen__movie_theater=theater).exclude(
            Q(pk=current_broadcast.pk) | Q(movie=current_broadcast.movie)
        )
        # same day - same hour - other screen
        q = base_q.filter(date=current_broadcast.date,
                          schedule=current_broadcast.schedule,
                          screen__pk__lt=current_broadcast.screen.pk
                          ).order_by('-screen__pk')
        if q.exists():
            return q.all()[0]
        # same day - other hour
        q = base_q.filter(date=current_broadcast.date,
                          schedule__time__lt=current_broadcast.schedule.time,
                          ).order_by('-schedule__time', '-screen__pk')
        if q.exists():
            return q.all()[0]
        # other day
        q = base_q.filter(date__lt=current_broadcast.date,
                          ).order_by('-date', '-schedule__time', '-screen__pk')
        if q.exists():
            return q.all()[0]
        return

    def get_next_broadcast(self, current_broadcast, theater):
        if not current_broadcast:
            return
        base_q = models.Broadcast.objects.filter(
            movie__available=True,
            screen__movie_theater=theater).exclude(
            Q(pk=current_broadcast.pk) | Q(movie=current_broadcast.movie)
        )
        # same day - same hour - other screen
        q = base_q.filter(date=current_broadcast.date,
                          schedule=current_broadcast.schedule,
                          screen__pk__gt=current_broadcast.screen.pk
                          ).order_by('screen__pk')
        if q.exists():
            return q.all()[0]
        # same day - other hour
        q = base_q.filter(date=current_broadcast.date,
                          schedule__time__gt=current_broadcast.schedule.time,
                          ).order_by('schedule__time', 'screen__pk')
        if q.exists():
            return q.all()[0]
        # other day
        q = base_q.filter(date__gt=current_broadcast.date,
                          ).order_by('date', 'schedule__time', 'screen__pk')
        if q.exists():
            return q.all()[0]
        return


class MovieDetailLink(MovieDetailMixin, Index):
    pass


class MovieDetail(MovieDetailMixin, TemplateView):
    template_name = "movie_detail.html"

    def get_context_data(self, **kwargs):
        return self.get_movie_context()


class EventDetailMixin(TheaterView):
    def get_event_detail(self):
        context = {}
        theater = self.get_theater()
        context['theater'] = theater

        q = models.Event.objects.filter(
            available=True, slug=self.kwargs.get('slug', None))
        if not q.count():
            raise Http404(_("This event is not available."))
        context['event'] = q.all()[0]
        context['share_message'] = _("{0} - {1}").format(
            context['theater'], context['event']
        )
        return context


class EventDetailLink(EventDetailMixin, Index):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.get_event_detail())
        return context


class EventDetail(EventDetailMixin, TemplateView):
    template_name = "event_detail.html"

    def get_context_data(self, **kwargs):
        return self.get_event_detail()


class ApiMovieDetail(generics.RetrieveAPIView):
    lookup_field = 'slug'
    serializer_class = serializers.MovieSerializer
    queryset = models.Movie.objects.filter(available=True)


class ApiBroadcastListRetrieve(generics.ListAPIView):
    serializer_class = serializers.BroadcastSerializer
    queryset = models.Broadcast.objects.filter(movie__available=True)

    def list(self, request, start_date=None, end_date=None):
        queryset = self.get_queryset()
        if not start_date:
            start_date = datetime.date.today()
        queryset = queryset.filter(date__gte=start_date)
        if end_date:
            queryset = queryset.filter(date__lte=end_date)
        serializer = serializers.BroadcastSerializer(queryset.distinct(),
                                                     many=True)
        return Response(serializer.data)


class ICSMovieView(MovieDetailMixin, View):
    """Returns an .ics file for the given movie"""
    http_method_names = [u'get', ]

    def get(self, request, *args, **kwargs):
        context = self.get_movie_context()
        if not context.get('current_broadcast', None):
            raise Http404(_("Broadcast not available."))

        broadcast = context.get('current_broadcast')
        theater = context['theater']

        cal = ical.Calendar()
        cal.add('prodid', '-//Fantasmagorie//Iggdrasil//{}'.format(
            settings.LANGUAGE_CODE.split('-')[0].upper()))
        cal.add('version', '2.0')
        event = ical.Event()
        event.add('summary', str(broadcast.movie))
        desc = broadcast.movie.description + "\n" + \
            broadcast.movie.technical_description

        url = str(context_processors.base(request)['current_url'])
        desc += "\n" + url + reverse(
            'movie-detail-link', args=[theater.slug, broadcast.movie.slug,
                                       broadcast.pk])
        event.add('description', desc)

        date = datetime.datetime(
            broadcast.date.year, broadcast.date.month, broadcast.date.day,
            broadcast.schedule.time.hour, broadcast.schedule.time.minute)
        event.add('dtstart', date)
        end = date
        if broadcast.movie.duration:
            # the broadcast should start 15 minutes after the indicated hour
            end += broadcast.movie.duration + datetime.timedelta(minutes=15)
        else:
            end += datetime.timedelta(hours=2)
        event.add('dtend', end)
        event.add('dtstamp', datetime.datetime.now())
        location = str(theater)
        if theater.town:
            location += ", " + theater.town
        event['location'] = ical.vText(location)
        event['uid'] = 'fantasmagorie-{}-{}-{}'.format(
            theater.slug, broadcast.movie.slug, broadcast.pk)
        event.add('priority', 5)
        event.add('status', 'TENTATIVE')

        cal.add_component(event)
        content = cal.to_ical()
        response = HttpResponse(content, content_type='text/calendar')
        response['Content-Disposition'] = 'attachment; filename=event.ics'
        return response


def signup(request, **kwargs):
    theater = get_theater(kwargs)
    if request.method == 'POST':
        form = forms.SignUpForm(request.POST, theater=theater)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = str(_('[{}] Activate your account')).format(theater)
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            form.save()
            return redirect('newsletter_login',
                            first='first')
    else:
        form = forms.SignUpForm(theater=theater)
    return render(request, 'registration/signup.html',
                  {'form': form, 'theater': theater.slug})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('newsletter-redirect')
    else:
        return render(request, 'account_activation_invalid.html')


@login_required
def delete_account(request, **kwargs):
    theater = get_theater(kwargs)
    Form = forms.DeleteAccountForm
    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            if form.cleaned_data["sure"] == True:
                pk = request.user.pk
                logout(request)
                User.objects.get(pk=pk).delete()
                return redirect('newsletter_login',
                                theater=theater.slug)
            return redirect('newsletter_detail',
                            newsletter_slug=theater.newsletter.slug)
    else:
        form = Form()
    return render(request, 'registration/delete_account.html', {'form': form})


def password_reset(request):
    return auth_views.password_reset(
        request,
        post_reset_redirect='f_password_reset_done',
        template_name='registration/f_password_reset_form.html'
    )


def password_reset_done(request):
    return auth_views.password_reset_done(
        request, template_name='registration/f_password_reset_done.html')


def newsletter_login(request, **kwargs):
    theater = get_theater(kwargs)
    first = kwargs.get('first', None)

    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('newsletter_detail',
                            newsletter_slug=theater.newsletter.slug)
    else:
        form = AuthenticationForm()
    return render(request, 'registration/login.html',
                  {'form': form, 'first': first, 'theater': theater.slug})


def newsletter_manager(request, **kwargs):
    theater = get_theater(kwargs)
    if not request.user.is_authenticated():
        return redirect('newsletter_login',
                        theater=theater.newsletter.slug)
    else:
        return redirect('newsletter_detail',
                        newsletter_slug=theater.newsletter.slug)


class NewsletterRedirect(Index):
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['show_newsletter'] = True
        messages.add_message(self.request, messages.INFO,
                             _("Your account have been activated"))
        return context


def unsubscribe(request, email=None):
    theater = get_theater({})
    dct = get_main_context(theater)
    dct['full_page'] = True
    initial = {}
    if email:
        email = unquote_plus(email)
        dct['email'] = email
        initial['email'] = email
        q = Subscription.objects.filter(email_field=email, subscribed=True)
        if not q.count():
            dct['unknown'] = True

    Form = forms.UnsubscribeForm
    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            email =  form.cleaned_data['email']
            dct['email'] = email
            q = Subscription.objects.filter(email_field=email, subscribed=True)
            if not q.count():
                dct['form'] = form
                dct['unknown'] = True
                return render(request, 'unsubscribe.html', dct)

            if form.cleaned_data['sure']:
                for s in q.all():
                    s.subscribed = False
                    s.save()
                dct['success'] = True
                return render(request, 'unsubscribe.html', dct)
    else:
        form = Form(initial=initial)
    dct['form'] = form
    return render(request, 'unsubscribe.html', dct)


def subscribe(request, **kwargs):
    theater = get_theater(kwargs)
    if not theater.newsletter:
        raise Http404()

    if request.method == 'POST':
        form = forms.SubscribeForm(request.POST, theater=theater)
        if form.is_valid():
            email = form.cleaned_data['email']
            q = Subscription.objects.filter(email_field=email,
                                            newsletter=theater.newsletter)
            if q.count():
                sub = q.all()[0]
                if sub.subscribed:
                    return render(
                        request, 'subscribe.html',
                        {'already': True, 'theater': theater.slug,
                         'coded_email': quote_plus(email),
                         'email': email,
                         'form': forms.SubscribeForm(theater=theater)})
                sub.subscribed = True
                sub.save()
            else:
                Subscription.objects.create(
                    name_field=form.cleaned_data['name'],
                    email_field=email, newsletter=theater.newsletter,
                    subscribed=True)
            return render(request, 'subscribe.html',
                          {'success': True, 'theater': theater.slug,
                           'newsletter_email': theater.newsletter.email,
                           'email': email})
    else:
        form = forms.SubscribeForm(theater=theater)
    return render(request, 'subscribe.html',
                  {'form': form, 'theater': theater.slug})

