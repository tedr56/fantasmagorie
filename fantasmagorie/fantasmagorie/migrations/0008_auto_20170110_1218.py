# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0007_auto_20161222_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='broadcasttag',
            name='slug',
            field=models.SlugField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', unique=True),
        ),
        migrations.AlterField(
            model_name='colorset',
            name='slug',
            field=models.SlugField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', unique=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='slug',
            field=models.SlugField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', unique=True),
        ),
        migrations.AlterField(
            model_name='genre',
            name='slug',
            field=models.SlugField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', unique=True),
        ),
        migrations.AlterField(
            model_name='movie',
            name='slug',
            field=models.SlugField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', unique=True),
        ),
        migrations.AlterField(
            model_name='movietag',
            name='slug',
            field=models.SlugField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', unique=True),
        ),
        migrations.AlterField(
            model_name='movietheater',
            name='slug',
            field=models.SlugField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', unique=True),
        ),
        migrations.AlterField(
            model_name='screen',
            name='slug',
            field=models.SlugField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', verbose_name='Slug', unique=True),
        ),
    ]
