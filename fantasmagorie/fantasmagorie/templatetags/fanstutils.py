from bs4 import BeautifulSoup
import re
import markdown

from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import date as _date

from figureAltCaption.figureAltCaption import FigureCaptionExtension

register = template.Library()

clean_n = re.compile('\n+')


@register.filter
def htmlify(content):
    content = BeautifulSoup(content, "html.parser").text
    content = re.sub(clean_n, '</p><p>', content).replace("<p></p>", '')
    content = "<p>" + content + "</p>"
    return mark_safe(content)


@register.filter
def duration(td):
    if not hasattr(td, 'total_seconds'):
        return ""
    total_seconds = int(td.total_seconds())
    hours = total_seconds // 3600
    minutes = (total_seconds % 3600) // 60
    if hours == 0:
        return _('{} mins').format(minutes)
    elif hours == 1:
        if minutes:
            return _('{} hour {} mins').format(hours, minutes)
        else:
            return _('{} hour').format(hours)
    if minutes:
        return _('{} hours {} mins').format(hours, minutes)
    return _('{} hours').format(hours)


@register.filter
def simple_date(date):
    if not hasattr(date, 'strftime'):
        return ""
    return _date(date, 'd b Y')


@register.filter
def markdownify(text):
    # safe_mode governs how the function handles raw HTML
    return mark_safe(markdown.markdown(
        text, extensions=[
            'markdown.extensions.attr_list',
            FigureCaptionExtension()]))


@register.filter
def demarkdownify(text):
    # remove markdown formating
    html = markdownify(text)
    return ''.join(BeautifulSoup(html).findAll(text=True))


@register.filter
def safe_page(page):
    if page.page_type == 'html':
        return mark_safe(page.content)
    elif page.page_type == 'md':
        return markdownify(page.content)
    return page.content
