# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0002_movietheater_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='color',
            field=models.ForeignKey(null=True, blank=True, verbose_name='Associated color', to='fantasmagorie.ColorSet'),
        ),
    ]
