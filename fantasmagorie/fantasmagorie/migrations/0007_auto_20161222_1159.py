# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0006_auto_20161222_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='broadcasttag',
            name='is_badge',
            field=models.BooleanField(verbose_name='Is a badge', default=False),
        ),
        migrations.AddField(
            model_name='movietag',
            name='is_badge',
            field=models.BooleanField(verbose_name='Is a badge', default=False),
        ),
    ]
