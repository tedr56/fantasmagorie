import csv
import datetime
import locale

from django.conf import settings
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.template import engines
from django.template.defaultfilters import date as date_filter
from django.utils.translation import ugettext_lazy as _

from newsletter.models import Message, Article

from fantasmagorie.templatetags.fanstutils import markdownify, duration


def text_split(text, max_length):
    final = ""
    counter = 0
    for word in text.split(' '):
        counter += len(word) + 1  # the space
        if counter > max_length:
            if counter:
                final += "\n" + word
                counter = len(word)
            else:  # a single word with a length > max_length
                final += word + "\n"
                counter = 0
        else:
            final += " " + word
    return final


def create_mailing_from_movies(queryset, theater, date, request=None,
                               slug=None):
    q = queryset.filter(pk__in=[m.pk for m in queryset.all()],
                        available=True,
                        broadcasts__pk__isnull=False).distinct()
    if not queryset.count() or q.count() != queryset.count():
        return _("Cannot create a mailing - all movies do not have an "
                 "associated broadcast or are not available.")

    # get the list of movie sorted by broadcast date
    # usually a newsletter talk about films to be broadcasted and are ordered
    # by broadcast date
    movies = []  # list of (first date, movie, broadcasts)
    for movie in q.all():
        q_broadcast = movie.broadcasts.filter(date__gte=date)
        if not q_broadcast.count():
            q_broadcast = movie.broadcasts
        movie_broadcasts = list(
            q_broadcast.order_by('date', 'schedule').all())
        movies.append((movie_broadcasts[0].date,
                       movie_broadcasts[0].schedule.time, movie,
                       movie_broadcasts))
    return create_mailing(theater, movies, request, slug=slug, date=date)


def create_mailing(theater, movies, request=None, slug=None, date=None):
    """
    Create a mailing from a list of movies

    :param theater: theater
    :param movies: list of (first date, schedule, movie, broadcasts)
    :param request: current request (if provided)
    :return: mailing if created
    """
    locale.setlocale(locale.LC_TIME, settings.FANTASMAGORIE_LOCALE)

    # for now we are considering that this movie is broadcast on a single
    # movie theater - multi-theater is not ready for now
    newsletter = theater.newsletter
    if not newsletter:
        if request:
            messages.error(request, _("Cannot create a mailing - No newsletter "
                                      "associated to the theater."))
        return
    if not slug:
        now = datetime.datetime.now()
        slug = theater.slug + "-" + now.strftime("%Y-%m-%d-%H-%M-%S")
    # create the message
    first_date = "..."
    last_date = "..."
    if date:
        first_date = date_filter(date)
        last_date = date_filter(date + datetime.timedelta(6))
    message = Message.objects.create(
        title=str(_("Week from {} to {}")).format(first_date, last_date),
        slug=slug,
        newsletter=newsletter)

    if request:
        site = get_current_site(request)
        protocol = 'https' if request.is_secure() else 'http'
    else:
        site = theater.site
        protocol = 'https' if theater.https_site else 'http'
    base_url = protocol + "://" + site.domain
    if base_url.endswith("/"):
        base_url = base_url[:-1]

    if theater.header_newsletter:
        article = Article(sortorder=1, title=_("Editorial"), post=message,
                          text=markdownify(theater.header_newsletter))
        article.save()

    django_engine = engines['django']
    # create an article for each movie
    for idx, movie in enumerate(sorted(movies, key=lambda x: (x[0], x[1]))):
        date, time, movie, broadcasts = movie
        c_url = base_url + reverse("movie-detail-link",
                                   kwargs={"slug": movie.slug,
                                           "broadcast_id": broadcasts[0].pk})
        rendered_broadcasts = ""
        for nb, broadcast in enumerate(broadcasts):
            cls = 'broadcasts '
            if nb % 2:
                cls += 'broadcast-even'
            else:
                cls += 'broadcast'
            rendered_broadcast = "<tr class='{}'><td colspan='2'>".format(cls)
            if broadcast.original_version:
                rendered_broadcast += "<span class='badge'>{}</span> ".format(
                    _("OV"))
            if broadcast.three_dimensional:
                rendered_broadcast += "<span class='badge'>{}</span> ".format(
                    _("3D"))
            rendered_broadcast += "{} - {} ".format(
                broadcast.date.strftime("%A %d %B %Y").capitalize(),
                broadcast.schedule)
            extra = ""
            for tag in broadcast.tags.all():
                if extra:
                    extra += " - "
                extra += tag.display
            for event in broadcast.events.all():
                if extra:
                    extra += " - "
                extra += event.short
            if broadcast.special_description:
                if extra:
                    extra += " - "
                extra += broadcast.special_description
            if extra:
                rendered_broadcast += "<br>\n" + extra
            rendered_broadcast += "</td></tr>\n"
            rendered_broadcasts += rendered_broadcast
        poster = ""
        if movie.poster and movie.poster.url:
            tpl_str = "{% load imagefit %}"
            tpl_str += '<td valign="top" class="poster"><img src="{{base_url}}'
            tpl_str += '{{poster_name|media_resize:\'mailing\'}}"/></td>'
            tpl = django_engine.from_string(tpl_str)
            poster = tpl.render({'poster_name': movie.poster.name,
                                 'base_url': base_url})

        tech = movie.technical_description or ""
        if movie.duration:
            tech = str(duration(movie.duration)) + " - " + tech
        if tech:
            tech = "<p><em>{}</em></p>\n\n".format(
                text_split(tech, 78))
        desc = markdownify(text_split(movie.description, 78))

        colspan = ""
        if not poster:
            colspan = " colspan='2'"
        text = "{}<tr>\n{}\n<td{} valign='top'>{}\n{}"\
               "</td></tr>".format(
            rendered_broadcasts, poster, colspan, tech, desc)
        if movie.special_description:
            text += "\n\n<tr><td colspan='2'><strong>{}" \
                    "</strong></td></tr>".format(
                text_split(movie.special_description, 78))

        article = Article(sortorder=idx + 10, title=movie.name, text=text,
                          url=c_url, post=message)
        article.save()

    if theater.footer_newsletter:
        article = Article(sortorder=100, title=theater.name, post=message,
                          text=markdownify(theater.footer_newsletter))
        article.save()
    if request:
        messages.info(request, _("Newsletter created."))
    return message


def export_as_csv_action(fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """

    def export_as_csv(modeladmin, request, queryset):
        opts = modeladmin.model._meta

        if not fields:
            field_names = [
                field.name for field in opts.fields
                if field.name not in exclude
            ]
        else:
            field_names = fields

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = \
            'attachment; filename=%s.csv' % str(opts).replace('.', '_')

        writer = csv.writer(response)
        if header:
            writer.writerow([str(_(name)).capitalize() for name in field_names])
        for obj in queryset:
            row = [
                getattr(obj, field)()
                if callable(getattr(obj, field)) else getattr(obj, field)
                for field in field_names
            ]
            writer.writerow(row)
        return response

    export_as_csv.short_description = _("Export as CSV file")

    return export_as_csv